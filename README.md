## Crypto aggregator

#### Intro

This project is meant to provide a command line tool that curates all trades across exchanges in a single CSV file using [ccxt](https://github.com/ccxt/ccxt/wiki/Manual) library where unified API is supported, otherwise instructions are provided for manual export. The table structure is chosen as flexible to be queried as SQL table, CSV, or as a pandas DataFrame.

The curated file can be later be loaded to [cryptosis](http://cryptosis.it) for visualisation purposes

#### List of exchanges supported

| Exchange    | Method         | Comments
| :---------- |:-------------- |:-------------------------------------------------------------------------------------------
| Binance     | API            | Add API keys under `config.ini`
| Kraken      | API            | Add API keys under `config.ini`
| Coinbase    | API            | Add API keys under `config.ini`
| Kucoin      | API            | Add API keys under `config.ini`
| Coinfloor   | Manual Export  | Exchange -> view previous trades -> export to email as CSV -> prefix file with `coinfloor_`
| Bittrex     | Manual Export  | Exchange -> Orders -> Download order history -> prefix file with `bittrex_`
| Litebit     | Manual Curation| See `export/sample.csv`, rename to `litebit_.csv`, change broker column to `litebit` and add manual entries
| Gatehub     | Manual Curation| See `export/sample.csv`, rename to `gatehub_.csv`, change broker column to `gatehub` and add manual entries
| ICOs        | Manual Curation| See `export/sample.csv`, rename to `ico_.csv`, change broker column to ico name and extend per ICO participation

##### Notes:
`Bittrex`
The exported file is encoded as UTF-16, not readable in Excel.
This is handled by the tool and saved as UTF-8 with `.fixed.` suffix.

`Kucoin`
API key is unnecessarily read/write, switch to read only not available yet.

`Litebit`
No export available.. add entries manually by viewing each order

##### Configuration per exchange

On exchanges that provide proper API support, add API keys under `config.ini`.
Alternatively follow instructions on table above for manual export and store the file under directory `export/`


## Data model

All trades curated from exchanges will be stored following table structure:

| Columns    | Type   | Description            |
| :--------  |:-------|:-----------------------|
| buyAsset   | str    | Coin symbol bought       |
| buyAmount  | float  | Number of coins bought   |
| sellAsset  | str    | Coin symbol sold         |
| sellAmount | float  | Number of coins sold     |
| date       | int    | Date in epoch format        |
| feesAsset  | str    | Coin symbol paid as transcation fee     |
| feesAmount | float  | Number of coins paid as transcation fee |
| broker     | str    | Broker name              |




# Run instructions

- brew install pipenv
- sh setup_pipenv.sh
- sh run.sh

# Output
```
export/
|___{exchange}_trades.csv
|___all_trades.csv
```

Under `export\` folder, each exchange will have a dedicated file containing trades, named `{exchange}_trades.csv`.

The file containing curated trades across exchnages is named `all_trades.csv`


#### ToDo:
- Re-create balance per exchange (does not include deposits/withdrawals, yet can be validated against balance across exchanges)
- Verify fees are set to right asset (especially Bittrex)
- Add BCH/BTG (or any other fork) in separate file
