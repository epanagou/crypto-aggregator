import utils
from termcolor import cprint
import pandas as pd
from state.DB import DB



def process_exchanges(exs):
    df_list = []
    print(exs)
    for exchange in exs:
        # Fetch context for exchange
        cprint("Fetching data for: {} ".format(exchange), 'white', 'on_blue')
        context = utils.fetch_context(exchange)
        if context:
            # Fetch trades for each exchange
            trades = context.fetch_trades()
            df_list.append(trades)
            # utils.display(trades)

    df_final = pd.concat(df_list, axis=0, ignore_index=True)
    utils.display(df_final)
    utils.df_to_csv(df_final, 'all')

    return df_final

if __name__== "__main__":

    cprint("Fetching exchange list..", 'yellow')
    exchanges = utils.fetch_exchanges()
    if exchanges:
        trades = process_exchanges(exchanges)
        # sys.exit(1)
        mydb = DB(trades)
        utils.display(mydb.balancesheet)
        # mydb.fetch_xtimes()

        # for ex in mydb.exchanges:
        #     print(ex)
        #     utils.display(mydb.fetch_balance(ex))

    # trades = pd.read_csv("ci_format/all_trades.csv")
    # mydb = DB(trades)
    # utils.display(mydb.balancesheet)






