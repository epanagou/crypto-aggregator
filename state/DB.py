import sys
import pandas as pd
import requests_cache
import state.func as utils

class DB():

    def __init__(self, orderbook):
        requests_cache.install_cache('cmc_cache', backend='sqlite', expire_after=36000)
        self.orderbook = orderbook
        self.balancesheet = self.fetch_balance()
        self.exchanges = orderbook.broker.unique().tolist()
        # self.xtimes = self.fetch_xtimes(orderbook)
        pd.set_option('display.float_format', lambda x: '%.3f' % x)



    def fetch_balance(self, exchange=None):
        # If no exchange passed, fetch total balance
        if not exchange:
            input_df = self.orderbook
        # Else get back balance for specifed exchange
        else:
            if exchange not in self.exchanges:
                print('No exchange with name {} found..'.format(exchange))
                return
            orb = self.orderbook
            input_df = orb[orb.broker == exchange]

        df_balance = self._calc_balance(input_df)
        return df_balance


    def _calc_balance(self, input_df):
        df_balance = pd.concat([input_df.groupby('buyAsset')['buyAmount'].sum(),
            input_df.groupby('sellAsset')['sellAmount'].sum()],
            #ignore_index=True,
            axis=1)

        df_balance.fillna(0, inplace=True)

        df_balance['balance'] = df_balance['buyAmount'] - df_balance['sellAmount']
        del df_balance['buyAmount']
        del df_balance['sellAmount']
        df_balance['symbol'] = df_balance.index
        df_balance = df_balance.sort_values('balance', ascending=False)

        df_balance = self._append_cmc(df_balance)

        return df_balance

    def _append_cmc(self, df_balance):
        df_cp = pd.read_json('https://api.coinmarketcap.com/v1/ticker/?start=0&limit=2000&convert=ETH')
        df_cp = df_cp[['name', 'rank','symbol', 'price_usd', 'price_btc', 'price_eth']]
        # utils.display(df_cp)
        df_cp = df_cp[df_cp.name != 'Bitgem']
        df_final = pd.merge(df_balance, df_cp, on='symbol')
        df_final['currentUSD'] = df_final['balance'] * df_final['price_usd']
        df_final['currentBTC'] = df_final['balance'] * df_final['price_btc']
        # df_final['currentETH'] = df_final['balance'] * df_final['price_eth']
        df_final.sort_values(['currentUSD'])

        current_total_usd = df_final['currentUSD'].sum()

        # Create percentage column
        df_final['percent'] = (df_final['currentUSD'] * 100) / current_total_usd
        df_final.sort_values('percent', inplace = True, ascending = False)

        df_final = df_final[df_final.balance > 3]
        print("Total (USD): {}".format(df_final.currentUSD.sum()))
        print("Total (BTC): {}".format(df_final.currentBTC.sum()))
        # print("Total (ETH): {}".format(df_final.currentETH.sum()))

        return df_final

    def fetch_xtimes(self, df_cryptosis):

        # unique coins
        fiat = ['USD', 'GBP', 'EUR']
        coins = set(df_cryptosis['buyAsset'].append(df_cryptosis['sellAsset']).values)

        # get trades for all except fiat until exposure is resolved
        coins = [x for x in coins if x not in fiat]
        coins_sample = ['NEO', 'OST', 'ADA', 'OMG']
        df_trades = utils.fetch_trades(df_cryptosis, coins )
        del df_trades['feesAsset']
        del df_trades['feesAmount']
        # utils.display(df_trades.head(5))

        # fix plus minus
        df_trades = utils.fix_plus_minus(df_trades)
        # utils.display(df_trades.head(5))

        # Add historic BTC,ETH equivalents that carry fixed plus/minus
        df_trades = utils.add_historic_columns(df_trades)
        # utils.display(df_trades.head(5))

        # Concatenate, get correct historic value as both BTC,ETH
        dd = df_trades.groupby(['buyAsset','sellAsset'], as_index=False).sum()
        del dd['date']
        # utils.display(dd.head(5))

        # Not working across trades, needs single symbol

        columns = ['buyAsset', 'dm_coin', 'buyAmount', 'sellAmount', 'sellAmount_historicBTC', 'sellAmount_historicETH', 'sellAmount_historicUSD']
        df_invest = pd.DataFrame(columns = columns)
        # find dominant coin
        for coin in coins:
            mask =  dd['buyAsset'] == coin
            dd_mask = dd[mask]
            idmax = dd_mask['buyAmount'].idxmax()
            # print(idmax)
            dd_mask['dm_coin'] = dd_mask.loc[idmax]['sellAsset']
            # utils.display(dd_mask)
            single_line = dd_mask.groupby(['buyAsset','dm_coin'], as_index=False).sum()
            # utils.display(single_line)
            df_invest = df_invest.append(single_line) #, ingore_index=True)
            # utils.display(single_line)


        # Change columns to align with balance sheet
        df_invest = utils.transform_names(df_invest)
        # utils.display(df_invest)

        # Fetch balance sheet
        df_bs, usd_sum = utils.fetch_balance_sheet(df_cryptosis)

        # merge
        df_final = pd.merge(df_invest, df_bs, on='symbol')


        # add xtimes
        df_final['xtimes'] = df_final.apply(utils.addxtimes, axis=1)

        # add bought_at
        # df_final['bought_at_BTC'] = df_final.apply(utils.bought_at_btc, axis=1)
        # df_final['bought_at_ETH'] = df_final.apply(utils.bought_at_eth, axis=1)

        # df_final.loc[df_final['dm_coin'] == 'BTC', 'price_bought_eth'] = '---'
        # # df_final.loc[df_final['dm_coin'] == 'BTC', 'price_bought_btc'] = df_final['invest_BTC'] / df_final['my_balance']
        # df_final.loc[df_final['dm_coin'] == 'ETH', 'price_bought_btc'] = '---'

        # df_final['price_bought_btc'] = df_final.apply(lambda x: (x['invest_BTC'] / x['my_balance']) if isinstance(x['invest_BTC'], float)  else '---' , axis=1 )
        # df_final['price_bought_eth'] = df_final.apply(lambda x: (x['invest_ETH'] / x['my_balance']) if (isinstance(x['invest_ETH'], float) & x['my_balance'] > 0)  else '---', axis=1 )

        # df_final[,'price_bought_btc'] = df_final.apply(lambda x: x['invest_BTC'] / x['my_balance'])

        df_final.to_csv("current.csv")
        # utils.display(df_final.sort_values('percent'))
        print(usd_sum)

        print("--PROBLEMATIC--")
        fiat_df = df_final.loc[~(df_final['my_balance'] == df_final['balance'])]
        utils.display(fiat_df)
        print(fiat_df['currentUSD'].sum())


        altcoin_df = df_final.loc[(df_final['my_balance'] == df_final['balance'])].sort_values('percent', ascending=False)
        altcoin_df = altcoin_df[altcoin_df['my_balance'] > 1]
        altcoin_df['bought_at_BTC']  = altcoin_df.apply(utils.bought_at_btc, axis=1)
        altcoin_df['bought_at_ETH']  = altcoin_df.apply(utils.bought_at_eth, axis=1)

        new_columns = ['symbol', 'xtimes', 'my_balance', 'bought_at_BTC', 'price_btc', 'bought_at_ETH', 'price_eth', 'price_usd', 'currentUSD' ]
        altcoin_df = altcoin_df.reindex(columns=new_columns)

        altcoin_df.loc[altcoin_df['bought_at_BTC'] == '---', 'price_btc'] = '---'
        altcoin_df.loc[altcoin_df['bought_at_ETH'] == '---', 'price_eth'] = '---'

        utils.display(altcoin_df)
        print(altcoin_df['currentUSD'].sum())
