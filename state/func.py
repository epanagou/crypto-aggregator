import pandas as pd
import numpy as np
import glob
import sys

from tabulate import tabulate
import requests
import requests_cache

def xtimes(current, bought_at):
  mformat = '%.03f' % float(current/bought_at)
    #   return 'x'+ format
  return mformat

def display(df):
    return print(tabulate(df, headers='keys', tablefmt='psql'))

def load_transactions(file):
    # Load data
    df_cryptosis= pd.read_csv(file)
    if 'Unnamed: 0' in df_cryptosis:
        del df_cryptosis['Unnamed: 0']
    df_cryptosis.dropna(how='all', inplace=True)
    df_cryptosis['date'] = df_cryptosis['date'].astype(int)
    return df_cryptosis

def fetch_trades(df, symbol_list):
    # if it's a string
    if not isinstance(symbol_list, list):
        symbol_list = [symbol_list]
    print(symbol_list)
    return df[(df['buyAsset'].isin(symbol_list)) | (df['sellAsset'].isin(symbol_list))]

def add_historic_columns(trades):
    # trades.is_copy = False

    trades['sellAmount_historicBTC'] = trades.apply(lambda x: x['sellAmount'] *
                            price_historic(x['sellAsset'], x['date'], astype = 'BTC'), axis=1 )
    trades['sellAmount_historicETH'] = trades.apply(lambda x: x['sellAmount'] *
                            price_historic(x['sellAsset'], x['date'], astype = 'ETH'), axis=1 )
    trades['sellAmount_historicUSD'] = trades.apply(lambda x: x['sellAmount'] *
                            price_historic(x['sellAsset'], x['date'], astype = 'USD'), axis=1 )
    # trades.loc['sellAmount_historicUSD'] = trades.apply(lambda x: x['sellAmount'] *
    #                         price_historic(x['sellAsset'], x['date'], astype = 'USD'), axis=1 )
    return trades

def minus_fees(trades):
    trades['buyAmount'] = trades['buyAmount'] - trades['feesAmount']
    del trades['feesAsset']
    del trades['feesAmount']

    return trades

def fix_plus_minus(trades):

    # If sold coins is NOT one of fiat/btc/eth, mark those as True :
    mask = ~trades['sellAsset'].isin(['USD', 'EUR', 'GBP', 'BTC', 'ETH'])
    old_sell_asset = trades.loc[mask, 'sellAsset']
    # display(trades[mask]['buyAsset'])
    # display(old_sell_asset)
    trades.loc[mask, 'sellAsset'] = trades[mask]['buyAsset']
    trades.loc[mask, 'buyAsset'] = old_sell_asset
    old_buy = trades.loc[mask, 'buyAmount']   #trades[mask]['buyAmount']
    old_sell = trades.loc[mask, 'sellAmount'] #trades[mask]['sellAmount']
    trades.loc[mask, 'sellAmount'] = old_buy.apply(lambda x : -x)
    trades.loc[mask, 'buyAmount'] = old_sell.apply(lambda x : -x)

    return trades

def price_historic(sellAsset, epoch, astype = 'BTC'):

    if not 'USD' in astype:
        astype2 = astype + ",USD"
    else:
        astype2 = sellAsset + "," + astype

    api_url = 'https://min-api.cryptocompare.com/data/pricehistorical?fsym='+sellAsset+'&tsyms=' + astype2 + '&ts='+str(epoch)
    # print(api_url)
    res = requests.get(api_url)
    # if res.from_cache:
    #     pass
    #     # print("Cache: " + api_url)
    # else:
    #     pass
    #     # print("now cached: "+ api_url)
    # print(res.json())
    # print(res.json()[sellAsset])
    # print(type(res.json()[sellAsset][astype]))
    return res.json()[sellAsset][astype]

def transform_names(df):
    df.loc[df['dm_coin'] == 'BTC', ['sellAmount_historicETH', 'sellAmount_historicUSD' ]] = '---'
    df.loc[df['dm_coin'] == 'ETH', ['sellAmount_historicBTC', 'sellAmount_historicUSD' ]] = '---'
    df.rename(columns={
        'buyAsset': 'symbol',
        'buyAmount': 'my_balance',
        'sellAmount_historicBTC': 'invest_BTC',
        'sellAmount_historicETH': 'invest_ETH',
        'sellAmount_historicUSD': 'invest_USD',
    }, inplace=True)
    del df['sellAmount']
    # del df['dm_coin']

    return df

def fetch_balance_sheet(orderbook):
    df_balance = pd.concat([orderbook.groupby('buyAsset')['buyAmount'].sum(),
                            orderbook.groupby('sellAsset')['sellAmount'].sum()],
                           #ignore_index=True,
                           axis=1)
    df_balance.fillna(0, inplace=True)
    df_balance['balance'] = df_balance['buyAmount'] - df_balance['sellAmount']
    del df_balance['buyAmount']
    del df_balance['sellAmount']
    df_balance['symbol'] = df_balance.index
    df_balance = df_balance.sort_values('balance', ascending=False)

    df_cp = pd.read_json('https://api.coinmarketcap.com/v1/ticker/?start=0&limit=2000&convert=ETH')
    df_cp = df_cp[['symbol', 'price_usd', 'price_btc', 'price_eth']] # 'rank',
    df_final = pd.merge(df_balance, df_cp, on='symbol')
    df_final['currentUSD'] = df_final['balance'] * df_final['price_usd']
    df_final['currentBTC'] = df_final['balance'] * df_final['price_btc']
    df_final['currentETH'] = df_final['balance'] * df_final['price_eth']

    df_final.sort_values(['currentUSD'])

    current_total_usd = df_final['currentUSD'].sum()
    # Create percentage column
    df_final['percent'] = (df_final['currentUSD'] * 100) / current_total_usd
    df_final.sort_values('percent', inplace = True, ascending = False)

    return df_final, current_total_usd

def xtimes(current, bought_at):
    format = '%.03f' % (current/float(bought_at))
    return 'x'+ format
    # return format

def addxtimes(row):
    if row['dm_coin'] == 'BTC':
        val = xtimes(row['currentBTC'], row['invest_BTC'])
    else:
        val = xtimes(row['currentETH'], row['invest_ETH'])
    return val

def bought_at_btc(row):
    if row['dm_coin'] == 'BTC' :
        # print(row['invest_BTC'])
        val = '%.08f' % float(row['invest_BTC'] / row['my_balance'])
    else:
        val = "---"
    return val

def bought_at_eth(row):
    if row['dm_coin'] == 'ETH' :
        # print(row['invest_ETH'])
        val = '%.08f' % float(row['invest_ETH'] / row['my_balance'])
    else:
        val = "---"
    return val
