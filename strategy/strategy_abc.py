from abc import ABCMeta, abstractmethod, abstractproperty

class AbsStrategy(metaclass=ABCMeta):
    @abstractmethod
    def fetch_trades(self):
        """ Required method, fetch trades aligned as cryprosis importer expects"""
        pass

    @abstractproperty
    def _name(self):
        """ Required property, retrieve exchange name"""
        pass

class Context:
    """
    Define the interface of interest to clients.
    Maintain a reference to a Strategy object.
    """

    def __init__(self, strategy):
        self._strategy = strategy

    def fetch_trades(self):
        return self._strategy.fetch_trades()
