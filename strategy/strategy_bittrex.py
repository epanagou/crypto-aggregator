import os
from strategy.strategy_abc import AbsStrategy
import ccxt
import time
import utils
import pandas as pd
import numpy as np
import json
from tqdm import tqdm
from termcolor import cprint
import glob
import sys

class BittrexStrategy(AbsStrategy):
    def fetch_trades(self):
        # If export exists, return existing trades
        trades_existing = utils.check_existing(self._name())
        if trades_existing:
            return pd.read_csv(trades_existing)

        exportfile = utils.get_csv(self._name())
        if exportfile:
            target = exportfile.replace(".csv", ".utf8.csv")
            targetUTF8 = self._read_binary(exportfile, target)
            df_temp = pd.read_csv(targetUTF8)
            del df_temp['OrderUuid']
            # utils.display(df_temp)
            df_trades = self._align_format(df_temp)
            # utils.display(df_trades)
            utils.df_to_csv(df_trades, self._name())
            return df_trades
        else:
            cprint("Error in locating CSV file from bittrex", 'red')
            sys.exit(1)

    def _name(self):
        return 'bittrex'

    def _align_format(self, df_temp):

        df_final = pd.DataFrame()
        df_final['buyAmount'] = np.where(df_temp['Type'].str.contains('BUY'), df_temp['Quantity'].astype('float64'), df_temp['Price'].astype('float64'))
        df_final['buyAsset'] = np.where(df_temp['Type'].str.contains('BUY'), df_temp['Exchange'].str.split('-').str.get(1), df_temp['Exchange'].str.split('-').str.get(0))
        # print(df_final['buyAsset'])
        df_final['sellAmount'] = np.where(df_temp['Type'].str.contains('SELL'), df_temp['Quantity'].astype('float64'), df_temp['Price'].astype('float64'))
        df_final['sellAsset'] = np.where(df_temp['Type'].str.contains('SELL'), df_temp['Exchange'].str.split('-').str.get(1), df_temp['Exchange'].str.split('-').str.get(0))
        # print(df_final['sellAsset'])
        df_final['date'] = df_temp['Opened'].apply(lambda x: self._to_epoch_bittrex(x)).astype('int64')

        df_final['feesAmount'] = df_temp['CommissionPaid'].astype('float64')
        df_final['feesAsset'] = df_temp['Exchange'].str.split('-').str.get(0)
        df_final['broker'] = self._name()

        df_final = df_final.replace("BCC", "BTC")

        return df_final

    def _read_binary(self, binaryfile, outfile):
        # If fixed file doesn't exist, create it and return
        if not os.path.isfile(outfile):
            with open(binaryfile, 'rb') as source_file:
                with open(outfile, 'w+b') as dest_file:
                    contents = source_file.read()
                    dest_file.write(contents.decode('utf-16').encode('utf-8'))
        return outfile

    def _to_epoch_bittrex(self,x):
        return time.mktime(time.strptime(x, '%m/%d/%Y %I:%M:%S %p'))
