import os
from strategy.strategy_abc import AbsStrategy
import ccxt
import time
import utils
import pandas as pd
import numpy as np
import json
from tqdm import tqdm
from coinbase.wallet.client import Client
from termcolor import cprint

class CoinbaseStrategy(AbsStrategy):
    def fetch_trades(self):
        # If export exists, return existing trades
        trades_existing = utils.check_existing(self._name())
        if trades_existing:
            return pd.read_csv(trades_existing)

        config = utils.read_config()
        api_key = config['EXCHANGE_API']['coinbase_api_key']
        api_secret = config['EXCHANGE_API']['coinbase_api_secret']

        #Coinbase client
        client = Client(api_key, api_secret)
        user = client.get_current_user()

        # final df to be populated
        df_list = []

        for account in client.get_accounts()["data"]:
            # Handle buys
            buys = client.get_buys(account["id"])["data"]
            # print(json.dumps(buys, indent=4))
            txs_buys = self.get_coinbase_buys(buys)

            if txs_buys:
                df2 = pd.DataFrame(txs_buys)
                df2['date'] = df2['date'].apply(self.dt_to_epoch_coinbase)
                df_list.append(df2)

            # Handle sells
            sells = client.get_sells(account["id"])["data"]
            # print(json.dumps(sells, indent=4))
            txs_sells = self.get_coinbase_sells(sells)

            if txs_sells:
                df2 = pd.DataFrame(txs_sells)
                df2['date'] = df2['date'].apply(self.dt_to_epoch_coinbase)
                df_list.append(df2)

        # Concat buys/sells df's
        df = pd.concat(df_list, ignore_index=True)

        # Edit dtypes
        df[['buyAmount', 'feesAmount', 'sellAmount']] = df[['buyAmount', 'feesAmount', 'sellAmount']].astype(float)
        df['date'] = df['date'].astype(int)

        utils.df_to_csv(df, self._name())

        return df

    def _name(self):
        return 'coinbase'

    def get_coinbase_buys(self, buys):
        txsList = []
        for buy in buys:
            if buy["status"] == "completed":
                # print(buy)
                tx = self.createTransactionDict(buy["amount"]["currency"],buy["amount"]["amount"],buy["total"]["currency"],buy["total"]["amount"],buy["fees"][0]["amount"]["currency"],buy["fees"][0]["amount"]["amount"],"Coinbase", buy["updated_at"])
                txsList.append(tx)
        return txsList

    def get_coinbase_sells(self, sells):
        txsList = []
        for sell in sells:
            if sell["status"] == "completed":
                # print(sell)
                tx = self.createTransactionDict(sell["subtotal"]["currency"],sell["subtotal"]["amount"],sell["amount"]["currency"],sell["amount"]["amount"],sell["fees"][0]["amount"]["currency"],sell["fees"][0]["amount"]["amount"],"Coinbase", sell["updated_at"])
                txsList.append(tx)
        return txsList

    def dt_to_epoch_coinbase(self, x):
        # return (x - dt.datetime(1970,1,1, hour=0, minute=0, second=0)).dt.total_seconds()
        return time.mktime(time.strptime(x, '%Y-%m-%dT%H:%M:%SZ'))

    def createTransactionDict(self, buyAsset,buyAmount, sellAsset,sellAmount, feesAsset, feesAmount, broker, when=None ):
        transaction = dict()
        transaction["buyAsset"] = buyAsset
        transaction["buyAmount"] = buyAmount
        transaction["sellAsset"] = sellAsset
        transaction["sellAmount"] = sellAmount
        transaction["feesAsset"] = feesAsset
        transaction["feesAmount"] = feesAmount
        transaction["broker"] = broker
        if when is None:
            transaction["date"] = calendar.timegm(time.gmtime())
        else:
            transaction["date"] = when


        return transaction
