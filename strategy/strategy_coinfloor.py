import os
from strategy.strategy_abc import AbsStrategy
import ccxt
import time
import utils
import pandas as pd
import numpy as np
import json
from tqdm import tqdm
from termcolor import cprint
import glob

class CoinfloorStrategy(AbsStrategy):
    def fetch_trades(self):
        # If export exists, return existing trades
        trades_existing = utils.check_existing(self._name())
        if trades_existing:
            return pd.read_csv(trades_existing)

        exportfile = utils.get_csv(self._name())
        if exportfile:
            df_temp = pd.read_csv(exportfile)
            # utils.display(df_temp)
            df_trades = self._align_format(df_temp)
            # utils.display(df_trades)
            utils.df_to_csv(df_trades, self._name())
            return df_trades
        else:
            cprint("Error in locating CSV file from Coinfloor", 'red')

    def _name(self):
        return 'coinfloor'

    def _align_format(self, df_temp):

        df_final = pd.DataFrame()
        df_final['buyAmount'] = df_temp['Amount'].astype('float64')
        df_final['buyAsset'] = 'BTC'
        # print(df_final['buyAsset'])
        df_final['sellAmount'] = df_temp['Total'].astype('float64')
        df_final['sellAsset'] = df_temp['Counter Asset']
        # print(df_final['sellAsset'])
        # df_temp['date'] = df_temp['Date & Time']
        # print(df_temp['Date & Time'].values)
        df_final['date'] = df_temp['Date & Time'].apply(lambda x: self._to_epoch_coinfloor(x)).astype('int64')

        df_final['feesAmount'] = 0
        df_final['feesAsset'] = 'BTC'
        df_final['broker'] = self._name()

        return df_final


    def _to_epoch_coinfloor(self,x):
        return time.mktime(time.strptime(x, '%Y-%m-%d %H:%M:%S %z'))
