import os
from strategy.strategy_abc import AbsStrategy
import ccxt
import time
import utils
import pandas as pd
import numpy as np
import json
from tqdm import tqdm
from termcolor import cprint
import glob

class GatehubStrategy(AbsStrategy):
    def fetch_trades(self):
        # If export exists, return existing trades
        trades_existing = utils.check_existing(self._name())
        if trades_existing:
            return pd.read_csv(trades_existing)

        exportfile = utils.get_csv(self._name())
        if exportfile:
            df_temp = pd.read_csv(exportfile)
            # No need to align, simply save with proper name
            # utils.display(df_temp)
            utils.df_to_csv(df_temp, self._name())
            return df_temp
        else:
            cprint("Error in locating CSV file from Gatehub", 'red')

    def _name(self):
        return 'gatehub'

