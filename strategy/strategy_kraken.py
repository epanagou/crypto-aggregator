import os
from strategy.strategy_abc import AbsStrategy
import ccxt
import time
import utils
import pandas as pd
import numpy as np
import json
from tqdm import tqdm
from termcolor import cprint

class KrakenStrategy(AbsStrategy):
    def fetch_trades(self):
        # If export exists, return existing trades
        trades_existing = utils.check_existing(self._name())
        if trades_existing:
            return pd.read_csv(trades_existing)

        kraken = ccxt.kraken()
        config = utils.read_config()
        kraken.apiKey = config['EXCHANGE_API']['kraken_api_key']
        kraken.secret = config['EXCHANGE_API']['kraken_api_secret']

        kraken_markets = kraken.load_markets()

        df = pd.DataFrame()
        appended_dfs = []

        # for symbol in tqdm(list(kraken_markets.keys())):

        # Every request to fetch_my_trades in kraken retrieves all trades, regardless of symbol
        time.sleep(kraken.rateLimit / 1000)
        trades = kraken.fetch_my_trades()
        if trades:
            # print(json.dumps(trades, indent=4))
            df_trades = self._align_format(trades)
            appended_dfs.append(df_trades)

        appended_dfs = pd.concat(appended_dfs, axis=0, ignore_index=True)
        utils.df_to_csv(appended_dfs, 'kraken')

        return appended_dfs

    def _name(self):
        return 'kraken'

    def _align_format(self, trades):
        df_temp = pd.read_json(json.dumps(trades), convert_dates=False)
        del df_temp['info']
        # utils.display(df_temp)
        df_final = pd.DataFrame()
        df_final['buyAmount'] = np.where(df_temp['side'].str.contains('buy'), df_temp['amount'].astype('float64'), (df_temp['price'] * df_temp['amount']).astype('float64'))
        df_final['buyAsset'] = np.where(df_temp['side'].str.contains('buy'), df_temp['symbol'].str.split('/').str.get(0), df_temp['symbol'].str.split('/').str.get(1))
        # print(df_final['buyAsset'])
        df_final['sellAmount'] = np.where(df_temp['side'].str.contains('sell'), df_temp['amount'].astype('float64'), (df_temp['price'] * df_temp['amount']).astype('float64'))
        df_final['sellAsset'] = np.where(df_temp['side'].str.contains('sell'), df_temp['symbol'].str.split('/').str.get(0), df_temp['symbol'].str.split('/').str.get(1))
        # print(df_final['sellAsset'])
        df_final['date'] = df_temp['timestamp'] #.apply(lambda x: x.to_pydatetime())

        df_final['feesAmount'] = df_temp['fee'].apply(lambda x : x['cost']).astype('float64')
        df_final['feesAsset'] = df_temp['fee'].apply(lambda x : x['currency'])
        df_final['broker'] = "kraken"

        return df_final

