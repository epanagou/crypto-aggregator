import os
from strategy.strategy_abc import AbsStrategy
import ccxt
import time
import utils
import pandas as pd
import numpy as np
import json
from tqdm import tqdm
from termcolor import cprint

class KucoinStrategy(AbsStrategy):
    def fetch_trades(self):
        # If export exists, return existing trades
        trades_existing = utils.check_existing(self._name())
        if trades_existing:
            return pd.read_csv(trades_existing)

        kucoin = ccxt.kucoin()
        config = utils.read_config()
        kucoin.apiKey = config['EXCHANGE_API']['kucoin_api_key']
        kucoin.secret = config['EXCHANGE_API']['kucoin_api_secret']

        kucoin_markets = kucoin.load_markets()

        df = pd.DataFrame()
        appended_dfs = []

        for symbol in tqdm(list(kucoin_markets.keys())):
            time.sleep(kucoin.rateLimit / 1000)
            trades = kucoin.fetch_my_trades(symbol)
            # print(symbol)
            # print(json.dumps(trades, indent=4))
            if trades:
                df_trades = self._align_format(trades)
                # Append each symbol df to final df
                appended_dfs.append(df_trades)

        appended_dfs = pd.concat(appended_dfs, axis=0, ignore_index=True)
        utils.df_to_csv(appended_dfs, self._name())

        return appended_dfs

    def _name(self):
        return 'kucoin'

    def _align_format(self, trades):
        df_temp = pd.read_json(json.dumps(trades), convert_dates=False)

        df_final = pd.DataFrame()
        df_final['buyAmount'] = np.where(df_temp['side'].str.contains('buy'), df_temp['amount'].astype('float64'), df_temp['cost'].astype('float64'))
        df_final['buyAsset'] = np.where(df_temp['side'].str.contains('buy'), df_temp['symbol'].str.split('/').str.get(0), df_temp['symbol'].str.split('/').str.get(1))
        # print(df_final['buyAsset'])
        df_final['sellAmount'] = np.where(df_temp['side'].str.contains('sell'), df_temp['amount'].astype('float64'), df_temp['cost'].astype('float64'))
        df_final['sellAsset'] = np.where(df_temp['side'].str.contains('sell'), df_temp['symbol'].str.split('/').str.get(0), df_temp['symbol'].str.split('/').str.get(1))
        # print(df_final['sellAsset'])
        df_final['date'] = df_temp['timestamp'] #.apply(lambda x: x.to_pydatetime())

        df_final['feesAmount'] = df_temp['fee'].apply(lambda x : x['cost']).astype('float64')
        df_final['feesAsset'] = df_temp['fee'].apply(lambda x : x['currency'])
        df_final['broker'] = self._name()

        return df_final

