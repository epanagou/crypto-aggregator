import os
from strategy.strategy_abc import AbsStrategy
import ccxt
import time
import utils
import pandas as pd
import numpy as np
import json
from tqdm import tqdm
from termcolor import cprint
import glob

class LitebitStrategy(AbsStrategy):
    def fetch_trades(self):
        # If export exists, return existing trades
        trades_existing = utils.check_existing(self._name())
        if trades_existing:
            return pd.read_csv(trades_existing)

        exportfile = utils.get_csv(self._name())
        if exportfile:
            df_temp = pd.read_csv(exportfile)
            df_trades = self._align_format(df_temp)
            utils.df_to_csv(df_trades, self._name())
            return df_trades
        else:
            cprint("Error in locating CSV file from Litebit", 'red')

    def _name(self):
        return 'litebit'

    def _align_format(self, df_temp):

        df_temp['date'] = df_temp['date'].apply(lambda x: self._to_epoch_litebit(x)).astype('int64')
        return df_temp

    def _to_epoch_litebit(self,x):
        return time.mktime(time.strptime(x, "%d/%m/%y %H:%M"))


