import os
import configparser
from tabulate import tabulate
import json
import pandas as pd
import numpy as np
import glob
import importlib
from strategy.strategy_abc import Context
from termcolor import cprint

def fetch_context(exchange_string):
    exStrategy = class_for_name(exchange_string)
    if exStrategy:
        return Context(exStrategy())

def class_for_name(ex):
    m = ()
    c = ()
    # load the module, will raise ImportError if module cannot be loaded
    try:
        m = importlib.import_module('strategy.strategy_{}'.format(ex))
    except ImportError:
        print("{} is not implemented..".format(ex.capitalize()))
        return
    # get the class, will raise AttributeError if class cannot be found
    try:
        c = getattr(m, "{}Strategy".format(ex.capitalize()))
    except AttributeError:
        cprint("{} is not implemented..".format(ex.capitalize()), 'red')
    return c

def read_config():
    # read config file
    config = configparser.ConfigParser()
    config.read('config.ini')
    return config

def fetch_exchanges():
    exs = set()

    #Read config.ini
    config = read_config()
    section = 'EXCHANGE_API'
    for option in config.options(section):
        # print(option)
        exs.add(option.split('_')[0])

    #Read csv files
    for filepath in glob.glob("export/*"):
        filename = filepath.split('/')[1]
        # print(filepath)
        if '_' in filename:
            exs.add(filename.split('_')[0])

    # Check if any exchanges were found
    if not exs:
        cprint("No exchange API keys found in config.ini", "red")
        cprint("No files located under  export/ folder", "red")
        return

    return exs

def get_csv(exchange):
    for filepath in glob.glob("export/*"):
        if (exchange in filepath) and not ('fixed' in filepath) and not ('sample' in filepath):
            return filepath

def df_to_csv(df, exchange):
    suffix = exchange+'_trades.csv'
    os.makedirs("./ci_format", exist_ok = True)
    df.to_csv('./ci_format/'+ suffix, index=False)
    cprint("Saved as {}".format(suffix), 'green')

def display(df):
    return print(tabulate(df, headers='keys', tablefmt='psql'))

def fetch_local(symbol):
    with open(self._name()+'_'+symbol.split('/')[0]+'.json', 'r') as exfile:
        return exfile.read()

def check_existing(exchange):
    if os.path.isfile("./ci_format/"+exchange+"_trades.csv"):
        cprint("Skipping, export exists..",'green')
        return "./ci_format/"+exchange+"_trades.csv"

